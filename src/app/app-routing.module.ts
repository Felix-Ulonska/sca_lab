import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DpaModule } from "./dpa/dpa.module";
import { CpaComponent } from "./dpa/pages/cpa/cpa.component";
import { DpaMainComponent } from "./dpa/pages/dpa-main/dpa-main.component";
import { PaddingMainComponent } from "./padding-oracle/_components/padding-main/padding-main.component";
import { MainComponent as MainRSATimingAttackComponent } from "./rsa-timing-attack/_components/main/main.component";

const routes: Routes = [
  { path: "", redirectTo: "/rsaTiming", pathMatch: "full" },
  { path: "rsaTiming", component: MainRSATimingAttackComponent },
  { path: "padding", component: PaddingMainComponent },
  { path: "dpa", component: DpaMainComponent },
  { path: "cpa", component: CpaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
