import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScaComparisionTraceComponent } from './sca-comparision-trace.component';

describe('ScaComparisionTraceComponent', () => {
  let component: ScaComparisionTraceComponent;
  let fixture: ComponentFixture<ScaComparisionTraceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScaComparisionTraceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScaComparisionTraceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
