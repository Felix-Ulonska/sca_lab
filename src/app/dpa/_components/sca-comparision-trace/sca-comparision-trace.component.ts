import { Component, Input, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { ChartConfiguration } from "chart.js";
import {
  combineLatest,
  debounceTime,
  distinct,
  filter,
  firstValueFrom,
  map,
  Observable,
  ReplaySubject,
  startWith,
  tap,
} from "rxjs";
import { Trace, Traces } from "../../_logic/Traces";

@Component({
  selector: "sca-comparision-trace",
  templateUrl: "./sca-comparision-trace.component.html",
  styleUrls: ["./sca-comparision-trace.component.scss"],
})
export class ScaComparisionTraceComponent {
  traces$ = new ReplaySubject<Trace[]>(1);

  traceSelecterFormControl = new FormControl<string>("0x00", (aControl) => {
    const errorExist = (aControl.value as string).split(",").some((val) => {
      const num = parseInt(val, 16);
      return isNaN(num) || num < 0 || num > this.maxLength;
    });
    if (errorExist) {
      return { errorMsg: "Format: 0x1b, 0xb3" };
    } else {
      return {};
    }
  });

  maxLength = 255;

  @Input()
  titleX: string | undefined = undefined;

  @Input()
  titleY: string | undefined = undefined;

  @Input()
  traces: Trace[] | undefined = undefined;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["traces"]?.previousValue !== changes["traces"].currentValue) {
      this.traces$.next(changes["traces"].currentValue);
    }
  }

  @Input()
  public lineChartOptions: ChartConfiguration["options"] = {
    scales: {
      x: {
        min: 0,
        max: 4000,
      },
    },
    animation: false,
  };

  currSelectedTraces$ = this.traceSelecterFormControl.valueChanges.pipe(
    startWith(this.traceSelecterFormControl.value),
    distinct(),
    debounceTime(10),
    map((val) => (val as string)!.split(",").map((num) => parseInt(num, 16))),
    tap((t) => console.log("trace select", t)),
    filter((val) => !val.some((num) => isNaN(num)))
  );

  lineChartData$: Observable<ChartConfiguration["data"]> = combineLatest([
    this.currSelectedTraces$,
    this.traces$.pipe(distinct()),
  ]).pipe(
    filter(([val1, val2]) => !!val2 && val2.length > 1),
    tap(([_, traces]) => (this.maxLength = traces?.length - 1)),
    map(([usedTracesIndex, traces]) => ({
      datasets: usedTracesIndex.map((i) => ({
        type: "line",
        label: `Trace ${i.toString(16)}`,
        data: traces[i].graphData.slice(),
        pointRadius: 0,
        borderWidth: 0.5,
        fill: "origin",
      })),
    }))
  );

  async add(trace: string) {
    const selectedTraces = this.traceSelecterFormControl.value
      ?.split(",")
      .map((num) => parseInt(num, 16));
    console.log(selectedTraces);
    if (!selectedTraces?.includes(parseInt(trace, 16)))
      this.traceSelecterFormControl.setValue(
        this.traceSelecterFormControl.value + ", " + trace
      );
  }
}
