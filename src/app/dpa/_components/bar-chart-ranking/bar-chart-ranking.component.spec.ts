import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartRankingComponent } from './bar-chart-ranking.component';

describe('BarChartRankingComponent', () => {
  let component: BarChartRankingComponent;
  let fixture: ComponentFixture<BarChartRankingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartRankingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BarChartRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
