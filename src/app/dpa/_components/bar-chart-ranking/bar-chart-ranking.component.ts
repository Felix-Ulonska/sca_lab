import { Component, Input } from "@angular/core";
import { ChartConfiguration, ChartData } from "chart.js";
import { DiffRet } from "../../_logic/Traces";

@Component({
  selector: "sca-bar-chart-ranking",
  templateUrl: "./bar-chart-ranking.component.html",
  styleUrls: ["./bar-chart-ranking.component.scss"],
})
export class BarChartRankingComponent {
  public data: ChartData<"bar", number[]> | null = null;
  @Input()
  public config: ChartConfiguration["options"] | null = {
    scales: {
      y: {
        beginAtZero: true,
        title: {
          text: "Max. Differenz",
          display: true,
        },
      },
      x: {
        title: {
          text: "Geratenes Keybyte",
          display: true,
        },
      },
    },
  };

  @Input() set diffResult(newRet: DiffRet[]) {
    const top5 = newRet.slice(0, 5);
    this.data = {
      labels: top5.map((val) => `0x${val.guess.toString(16)}`),
      datasets: [
        {
          label: "Top 5",
          data: top5.map((val) => val.diff.max),
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(54, 162, 235, 0.2)",
          ],
          borderColor: [
            "rgb(255, 99, 132)",
            "rgb(255, 159, 64)",
            "rgb(255, 205, 86)",
            "rgb(75, 192, 192)",
            "rgb(54, 162, 235)",
          ],
          borderWidth: 1,
        },
      ],
    };
    console.log(this.data);
  }

  @Input() set guesses(newRet: number[]) {
    const top5 = newRet
      .map((val, i) => ({
        key: i,
        val,
      }))
      .sort((a, b) => b.val - a.val)
      .slice(0, 5);
    this.data = {
      labels: top5.map((val, i) => `0x${val.key.toString(16)}`),
      datasets: [
        {
          label: "Top 5",
          data: top5.map((val) => val.val),
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(54, 162, 235, 0.2)",
          ],
          borderColor: [
            "rgb(255, 99, 132)",
            "rgb(255, 159, 64)",
            "rgb(255, 205, 86)",
            "rgb(75, 192, 192)",
            "rgb(54, 162, 235)",
          ],
          borderWidth: 1,
        },
      ],
    };
    console.log(this.data);
  }
}
