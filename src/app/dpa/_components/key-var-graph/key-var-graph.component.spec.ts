import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyVarGraphComponent } from './key-var-graph.component';

describe('KeyVarGraphComponent', () => {
  let component: KeyVarGraphComponent;
  let fixture: ComponentFixture<KeyVarGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeyVarGraphComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KeyVarGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
