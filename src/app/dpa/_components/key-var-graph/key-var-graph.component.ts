import { ChangeDetectorRef, Component } from "@angular/core";
import npyjs, { NPData } from "../../_logic/load_npjs";
import { Trace, Traces } from "../../_logic/Traces";

@Component({
  selector: "sca-key-var-graph",
  templateUrl: "./key-var-graph.component.html",
  styleUrls: ["./key-var-graph.component.scss"],
})
export class KeyVarGraphComponent {
  public trace: Trace | null = null;
  constructor(private cdr: ChangeDetectorRef) {
    new npyjs().load("/assets/aes_var_key.npy").then((traces) => {
      this.trace = new Traces(traces as NPData<Float64Array>, {}).getVariance();
      this.trace.rawData[0] = 0;
      this.cdr.markForCheck();
    });
  }
}
