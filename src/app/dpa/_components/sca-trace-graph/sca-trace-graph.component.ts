import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { ChartConfiguration } from "chart.js";
import { distinct, map, ReplaySubject } from "rxjs";
import { Observable } from "rxjs/internal/Observable";
import { Trace } from "../../_logic/Traces";

@Component({
  selector: "sca-trace-graph",
  templateUrl: "./sca-trace-graph.component.html",
  styleUrls: ["./sca-trace-graph.component.scss"],
})
export class ScaTraceGraphComponent implements OnChanges {
  trace$ = new ReplaySubject<Trace>(1);

  @Input()
  trace: Trace | undefined = undefined;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["trace"]?.previousValue !== changes["trace"].currentValue) {
      console.log(changes["trace"].currentValue);
      this.trace$.next(changes["trace"].currentValue);
    }
  }

  @Input()
  public lineChartOptions: ChartConfiguration["options"] = {
    scales: {
      x: {
        min: 0,
        max: 4000,
      },
    },
  };

  lineChartData$: Observable<ChartConfiguration["data"]> = this.trace$.pipe(
    distinct(),
    map(
      (trace) =>
        ({
          datasets: [
            {
              type: "line",
              data: trace.graphData.slice(),
              label: trace.graphSettings.label,
              backgroundColor: "rgba(77,83,96,0.2)",
              borderColor: "rgba(77,83,96,1)",
              pointBackgroundColor: "rgba(77,83,96,1)",
              pointBorderColor: "#fff",
              pointRadius: 0,
              borderWidth: 0.5,
              pointHoverBackgroundColor: "#fff",
              pointHoverBorderColor: "rgba(77,83,96,1)",
              fill: "origin",
            },
          ],
        } as ChartConfiguration["data"])
    )
  );
}
