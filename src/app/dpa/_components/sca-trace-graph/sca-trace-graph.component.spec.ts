import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScaTraceGraphComponent } from './sca-trace-graph.component';

describe('ScaTraceGraphComponent', () => {
  let component: ScaTraceGraphComponent;
  let fixture: ComponentFixture<ScaTraceGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScaTraceGraphComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScaTraceGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
