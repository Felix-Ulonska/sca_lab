/// <reference lib="webworker" />

import { Textin } from "./_logic/Textin";
import { Trace, Traces } from "./_logic/Traces";

export interface TraceWork {
  byteindex: number;
  bitnum: number;
  traces: Traces;
  textin: Textin;
}

addEventListener("message", async ({ data }) => {
  const args = data as TraceWork;
  args.traces = new Traces(args.traces.data, args.traces.opts);
  args.textin = new Textin(args.textin.textin);
  console.time("Attack");
  const resultsGuesses = [];
  for (let guess = 0; guess < 256; guess++) {
    const result = args.traces.calculateDiff(
      guess,
      args.textin,
      data.byteindex,
      data.bitnum
    );
    resultsGuesses.push(result);
    postMessage({ update: "update", guess });
  }
  resultsGuesses.sort((a, b) => b.diff.max - a.diff.max);
  console.timeEnd("Attack");
  postMessage(resultsGuesses);
});
