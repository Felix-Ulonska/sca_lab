import { Options } from "@angular-slider/ngx-slider/options";
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ChartConfiguration } from "chart.js";
import {
  BehaviorSubject,
  combineLatest,
  firstValueFrom,
  map,
  Observable,
  Observer,
  ReplaySubject,
  Subject,
  takeUntil,
} from "rxjs";
import { ScaComparisionTraceComponent } from "../../_components/sca-comparision-trace/sca-comparision-trace.component";
import { Cpa } from "../../_logic/cpa";
import { Trace } from "../../_logic/Traces";

@Component({
  selector: "sca-cpa",
  templateUrl: "./cpa.component.html",
  styleUrls: ["./cpa.component.scss"],
})
export class CpaComponent implements OnInit, OnDestroy {
  firstTrace = new ReplaySubject<Trace>(1);
  public attack = new Cpa();
  correlations$ = new ReplaySubject<Trace[]>(1);
  guesses$ = new ReplaySubject<number[]>(1);

  public barChartConfig: ChartConfiguration["options"] | null = {
    scales: {
      y: {
        beginAtZero: true,
        title: {
          text: "Max Korrelation",
          display: true,
        },
      },
      x: {
        title: {
          text: "Geratenes Keybyte",
          display: true,
        },
      },
    },
  };

  @ViewChild("scaCompGraph") scaComp: ScaComparisionTraceComponent | undefined;

  public traces: Trace[] = [];
  public destroy$ = new Subject<void>();

  attackRunning = new BehaviorSubject<boolean>(false);

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get firstTrace$(): Observable<Trace> {
    return this.attack.traces$.pipe(
      map((traces) => traces.getTrace(0))
    ) as Observable<Trace>;
  }

  sliderForm = new FormGroup({
    sliderControl: new FormControl<number>(4000),
    keyControl: new FormControl<number>(0),
  });

  options: Options = {
    floor: 0,
    ceil: this.traces.length - 1,
    step: 20,
  };

  keyoptions: Options = {
    floor: 0,
    ceil: 16,
    step: 1,
  };

  public preAttacklineChartOptions: ChartConfiguration["options"] = {
    scales: {
      x: {
        min: 0,
        max: 4000,
        title: {
          text: "Zeit",
          display: true,
          font: {
            size: 18,
          },
        },
      },
      y: {
        title: {
          text: "Leistung",
          display: true,
          font: {
            size: 18,
          },
        },
      },
    },
    animation: false,
  };

  public postAttacklineChartOptions: ChartConfiguration["options"] = {
    scales: {
      x: {
        min: 0,
        max: 4000,
        title: {
          text: "Zeit",
          display: true,
          font: {
            size: 18,
          },
        },
      },
      y: {
        title: {
          text: "Korrelation zu Model",
          display: true,
          font: {
            size: 18,
          },
        },
      },
    },
    animation: false,
  };

  ngOnInit() {
    this.attack.traces$.subscribe((traces) => {
      this.traces = traces.toArray();
      this.options = {
        floor: 0,
        ceil: this.traces.length - 1,
      };
      this.cdr.detectChanges();
    });
    this.bestCorrelation$
      .pipe(takeUntil(this.destroy$))
      .subscribe((bestCorrelation$) => {});
  }

  get bestCorrelation$(): Observable<Trace> {
    return combineLatest([this.correlations$, this.guesses$]).pipe(
      map(([correlations, guesses]) => {
        return correlations[new Trace(new Float64Array(guesses)).argMax];
      })
    );
  }

  get bestCorrelationGuess$(): Observable<{ trace: Trace; guess: number }> {
    return combineLatest([this.correlations$, this.guesses$]).pipe(
      map(([correlations, guesses]) => {
        return {
          trace: correlations[new Trace(new Float64Array(guesses)).argMax],
          guess: new Trace(new Float64Array(guesses)).argMax,
        };
      })
    );
  }

  known_key = [
    0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88,
    0x09, 0xcf, 0x4f, 0x3c,
  ];

  attackedByte = 0;

  isCorrect$ = this.bestCorrelationGuess$.pipe(
    map(({ trace, guess }) => guess === this.known_key[this.attackedByte])
  );

  async doAttack(): Promise<void> {
    this.attackRunning.next(true);
    const useTraces = this.sliderForm.get("sliderControl")?.value;
    console.log(useTraces);
    this.attackedByte = this.sliderForm.get("keyControl")!.value!;
    const { correlations, guesses } = await this.attack.attack({
      bNum: this.attackedByte,
      useTracesAmount: useTraces ?? 50,
    });
    this.attackRunning.next(false);
    this.correlations$.next(correlations);
    this.guesses$.next(Array.from(guesses));
    const bestGuess = await firstValueFrom(this.bestCorrelationGuess$);
    setTimeout(() => {
      this.scaComp!.add("0x" + bestGuess.guess.toString(16));
    }, 100);
    this.cdr.detectChanges();
  }
}
