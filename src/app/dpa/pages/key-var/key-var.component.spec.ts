import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyVarComponent } from './key-var.component';

describe('KeyVarComponent', () => {
  let component: KeyVarComponent;
  let fixture: ComponentFixture<KeyVarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeyVarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KeyVarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
