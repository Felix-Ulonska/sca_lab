import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { DpaAttack } from "../../_logic/dpa";
import { Options } from "@angular-slider/ngx-slider";
import {
  combineLatest,
  debounceTime,
  distinct,
  filter,
  firstValueFrom,
  map,
  Observable,
  startWith,
  Subject,
  takeUntil,
} from "rxjs";
import { DiffRet, Trace } from "../../_logic/Traces";
import { ChartConfiguration } from "chart.js";
import { ScaComparisionTraceComponent } from "../../_components/sca-comparision-trace/sca-comparision-trace.component";

@Component({
  selector: "sca-dpa-main",
  templateUrl: "./dpa-main.component.html",
  styleUrls: ["./dpa-main.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DpaMainComponent implements OnInit, OnDestroy {
  public attack = new DpaAttack();

  traces$ = this.attack.traces$;
  variance$: Observable<Trace> = this.attack.variance$;

  public traceSelectorFormControl = new FormControl<number>(0);

  @ViewChild("scaCompGraph") scaCompGraph:
    | ScaComparisionTraceComponent
    | undefined;

  currentTrace = combineLatest([
    this.traceSelectorFormControl.valueChanges.pipe(startWith(0)),
    this.traces$,
  ]).pipe(
    map(([currentSelection, traces]) => {
      const trace = traces.getTrace(currentSelection ?? 0);
      trace.graphSettings.label = `Trace ${currentSelection}`;
      return trace;
    })
  );

  getDiffTrace$ = this.attack.currDiffTraces$.pipe(
    map((traces) =>
      traces
        .slice(0)
        .sort((a, b) => a.guess - b.guess)
        .map((value) => value.diff)
    )
  );

  sliderForm = new FormGroup({});

  public currByteNum = 0;

  constructor(private cdr: ChangeDetectorRef) {}

  public lineChartOptions: ChartConfiguration["options"] = {
    scales: {
      x: {
        min: 0,
        max: 4000,
        ticks: {
          callback: (value, index, ticks) => `${value}`,
        },
        title: {
          text: "Zeit",
          display: true,
          font: {
            size: 18,
          },
        },
      },
      y: {
        ticks: {
          callback: (value, index, ticks) => `${value}`,
        },
        title: {
          text: "Leistung",
          display: true,
          font: {
            size: 18,
          },
        },
      },
    },
    animation: false,
  };

  public lineChartOptionsDiff: ChartConfiguration["options"] = {
    scales: {
      x: {
        min: 0,
        max: 4000,
        ticks: {
          callback: (value, index, ticks) => `${value}`,
        },
        title: {
          text: "Zeit",
          display: true,
          font: {
            size: 18,
          },
        },
      },
      y: {
        ticks: {
          callback: (value, index, ticks) => `${value}`,
        },
        title: {
          text: "Differenz",
          display: true,
          font: {
            size: 18,
          },
        },
      },
    },

    animation: false,
  };

  options: Options = {
    floor: 0,
    ceil: 4000,
    step: 20,
  };

  optionsBitSelect: Options = {
    floor: 0,
    ceil: 16,
  };

  optionsTraceUsage: Options = {
    floor: 0,
    ceil: 1000,
  };

  public traces: Trace[] = [];

  public formGroupSettings = new FormGroup({
    byteIndex: new FormControl<number>(0),
    numberOfTraces: new FormControl<number>(1000),
    sliderControl: new FormControl<number[]>([0, 4000]),
  });

  known_key = [
    0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88,
    0x09, 0xcf, 0x4f, 0x3c,
  ];

  destroy$ = new Subject<void>();

  ngOnInit(): void {
    this.traces$.subscribe((traces) => {
      this.traces = traces.toArray();
      this.cdr.detectChanges();
    });
    this.formGroupSettings.valueChanges
      .pipe(
        filter((val) => !!val),
        takeUntil(this.destroy$),
        debounceTime(150)
      )
      .subscribe((val) => {
        console.log("slidercontrol", val);
        this.attack.loadData({
          min: val.sliderControl![0],
          max: val.sliderControl![1],
          amountUsedTraces: val.numberOfTraces ?? undefined,
        });
      });
  }

  get isCorrect$() {
    return this.attack.currDiffTraces$.pipe(
      map((diffRet: DiffRet[]) => {
        return diffRet[0].guess === this.known_key[this.currByteNum];
      })
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public async doAttack() {
    const newByteNum = this.formGroupSettings.get("byteIndex")?.value!;
    this.attack
      .runAttack({
        byteNum: this.formGroupSettings.get("byteIndex")?.value!,
      })
      .then(() => {
        this.currByteNum = newByteNum;
        setTimeout(() => {
          firstValueFrom(
            this.attack.currDiffTraces$.pipe(
              map((diffRet: DiffRet[]) => {
                return diffRet[0].guess;
              })
            )
          ).then((bestGuess) =>
            this.scaCompGraph!.add("0x" + bestGuess.toString(16))
          );
        }, 100);
      });
  }
}
