import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DpaMainComponent } from './dpa-main.component';

describe('DpaMainComponent', () => {
  let component: DpaMainComponent;
  let fixture: ComponentFixture<DpaMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DpaMainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DpaMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
