import { sbox } from "./sbox";

export function aesInternal(inputdata: number, key: number): number {
  return sbox.at(inputdata ^ key)!;
}
