import { BehaviorSubject, filter, firstValueFrom, map, Observable } from "rxjs";
import { aesInternal } from "./AES";
import npyjs, { NPData } from "./load_npjs";
import { Textin } from "./Textin";
import { Trace, Traces } from "./Traces";

export class Cpa {
  private _traces$ = new BehaviorSubject<Traces | null>(null);
  public traces$ = this._traces$.pipe(
    filter((val) => !!val)
  ) as Observable<Traces>;

  public variance$ = this._traces$.pipe(
    filter((val) => !!val),
    map((val) => val?.getVariance())
  ) as Observable<Trace>;

  public hwLookUpTable: number[] = [];

  public _textin$ = new BehaviorSubject<Textin | null>(null);
  public textin$ = this._textin$.pipe(
    filter((val) => !!val)
  ) as Observable<Textin>;

  constructor() {
    for (let i = 0; i < 256; i++) {
      this.hwLookUpTable.push(i.toString(2).replaceAll("0", "").length);
    }
    this.loadData({});
  }

  async loadData(opts: { min?: number; max?: number }) {
    const textin: NPData<Uint8Array> = (await new npyjs().load(
      "/assets/lab4_2_textin.npy"
    )) as any;
    const traces = new Traces(
      (await new npyjs().load(
        "/assets/lab4_2_traces.npy"
      )) as NPData<Float64Array>,
      opts
    );
    this._textin$.next(new Textin(textin));
    this._traces$.next(traces);
  }

  stddev(X: number[], x_bar: number) {
    let sum = 0;
    for (let i = 0; i < X.length; i++) {
      sum += (X[i] - x_bar) ** 2;
    }
    return Math.sqrt(sum);
  }

  cov(trace_array: Traces, hws: number[], hws_bar: number) {
    const variance_traces = trace_array.getStdDev();
    const hws_variance = this.stddev(hws, hws_bar);
    let retVal = [];
    for (let col = 0; col < trace_array.traceLength; col++) {
      let sum = 0;
      let mean = 0;
      for (let row = 0; row < trace_array.length; row++) {
        mean += trace_array.instantAccess(row, col)!;
      }
      mean /= trace_array.length;
      for (let row = 0; row < trace_array.length; row++) {
        const val = trace_array.instantAccess(row, col);
        sum += (val - mean) * (hws[row] - hws_bar);
      }
      retVal.push(Math.abs(sum / (variance_traces.at(col)! * hws_variance)));
    }
    return new Trace(new Float64Array(retVal));
  }

  mean(X: number[]): number {
    let sum = 0;
    for (let j = 0; j < X.length; j++) {
      sum += X[j];
    }
    sum /= X.length;
    return sum;
  }

  async attack(opts: { useTracesAmount: number; bNum: number }): Promise<{
    correlations: Trace[];
    guesses: Float64Array;
  }> {
    const bnum = opts.bNum;
    const traces = (
      (await firstValueFrom(
        this._traces$.pipe(filter((val) => !!val))
      )) as Traces
    ).slice(0, opts.useTracesAmount + 1);
    const textin = (await firstValueFrom(
      this._textin$.pipe(filter((val) => !!val))
    )) as Textin;

    let correlations: Trace[] = [];

    const guesses = new Float64Array(255);
    for (let kguess = 0; kguess < 255; kguess++) {
      if (kguess % 20 == 0)
        await new Promise<void>((resolve) => setTimeout(() => resolve(), 0));
      const hws: number[] = [];
      for (let i = 0; i < traces!.length; i++) {
        hws.push(
          this.hwLookUpTable[aesInternal(textin.at(i)!.at(bnum)!, kguess)]
        );
      }
      const hws_bar = this.mean(hws);
      const cov = this.cov(traces, hws, hws_bar);
      guesses[kguess] = cov.max;
      correlations.push(cov);
      console.log(`kGuess: ${kguess.toString(16)} with ${cov.max}`);
    }
    return { correlations, guesses };
  }
}
