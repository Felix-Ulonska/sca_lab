import {
  BehaviorSubject,
  filter,
  firstValueFrom,
  map,
  Observable,
  tap,
} from "rxjs";
import npyjs, { NPData } from "./load_npjs";
import { Textin } from "./Textin";
import { DiffRet, Trace, Traces } from "./Traces";

/**
 *
 */
export class DpaAttack {
  private _traces$ = new BehaviorSubject<Traces | null>(null);
  public traces$ = this._traces$.pipe(
    filter((val) => !!val)
  ) as Observable<Traces>;

  public variance$ = this._traces$.pipe(
    filter((val) => !!val),
    map((val) => val?.getVariance())
  ) as Observable<Trace>;

  public _textin$ = new BehaviorSubject<Textin | null>(null);
  public textin$ = this._textin$.pipe(
    filter((val) => !!val)
  ) as Observable<Textin>;

  public worker = new Worker(new URL("../dpa-worker.worker", import.meta.url));

  public _currDifTraces$ = new BehaviorSubject<DiffRet[] | null>(null);
  public currDiffTraces$ = this._currDifTraces$.pipe(
    filter((val) => !!val)
  ) as Observable<DiffRet[]>;

  public isAttackRunning$ = new BehaviorSubject(false);
  public attackProgress$ = new BehaviorSubject<number>(0);

  constructor() {
    this.loadData({});
  }

  async loadData(opts: {
    min?: number;
    max?: number;
    amountUsedTraces?: number;
  }) {
    const textin: NPData<Uint8Array> = (await new npyjs().load(
      "/assets/dpa/textin_array.npy"
    )) as any;
    let traces = new Traces(
      (await new npyjs().load(
        "/assets/dpa/traces_aes.npy"
      )) as NPData<Float64Array>,
      opts
    );
    if (opts.amountUsedTraces) {
      traces = traces.slice(0, opts.amountUsedTraces);
    }
    this._textin$.next(new Textin(textin));
    this._traces$.next(traces);
  }

  async runAttack(opt: { byteNum: number }): Promise<void> {
    if (!this.isAttackRunning$.value) {
      this.isAttackRunning$.next(true);
      return new Promise<void>(async (resolve) => {
        this.worker.onmessage = ({ data }) => {
          if (data.update) {
            this.attackProgress$.next(data.guess);
          } else {
            const result = data as DiffRet[];
            this._currDifTraces$.next(
              result.map(
                (r) =>
                  ({
                    diff: new Trace(r.diff.trace_data, 0),
                    one_avg: new Trace(r.one_avg.trace_data, 0),
                    zero_avg: new Trace(r.zero_avg.trace_data, 0),
                    guess: r.guess,
                  } as DiffRet)
              )
            );
            this.isAttackRunning$.next(false);
            resolve();
          }
        };
        console.log(await firstValueFrom(this.traces$));
        this.worker.postMessage({
          byteindex: opt.byteNum,
          bitnum: 0,
          traces: await firstValueFrom(this.traces$),
          textin: await firstValueFrom(this.textin$),
        });
      });
    }
  }
}
