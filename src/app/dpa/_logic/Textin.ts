import { NPData } from "./load_npjs";

export class Textin {
  constructor(public readonly textin: NPData<Uint8Array>) {}

  get textLength(): number {
    return this.textin.shape[1];
  }

  get length(): number {
    return this.textin.shape[0];
  }

  at(i: number): Uint8Array {
    return this.textin.data.slice(
      this.textLength * i,
      this.textLength * (i + 1)
    );
  }
}
