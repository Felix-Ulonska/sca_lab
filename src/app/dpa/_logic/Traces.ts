import { aesInternal } from "./AES";
import { NPData } from "./load_npjs";
import { Textin } from "./Textin";

export class Traces {
  traceRange = [0, 4000];

  constructor(
    public data: NPData<Float64Array>,
    public opts: { min?: number; max?: number }
  ) {
    this.traceRange = [opts.min ?? 0, opts.max ?? this.traceLength];
  }

  get traceLength(): number {
    return this.data.shape[1];
  }

  get length(): number {
    return this.data.shape[0];
  }

  public getTrace(i: number): Trace {
    return new Trace(
      this.data.data.slice(
        i * this.traceLength + this.traceRange[0],
        (i + 1) * this.traceLength - (this.traceLength - this.traceRange[1])
      ),
      this.traceRange[0]
    );
  }

  public slice(start: number, end: number) {
    let newTraces = { ...this.data };
    newTraces.data = this.data.data.slice(
      start * this.traceLength,
      end * this.traceLength
    );
    this.data.shape[0] = end - start;
    return new Traces(newTraces, this.opts);
  }

  instantAccess(trace: number, i: number) {
    return this.data.data[trace * this.traceLength + this.traceRange[0] + i];
  }

  meanOfIds(ids: number[] | undefined = undefined): Trace {
    const mean = new Float64Array(this.traceLength);
    let id_length = ids?.length ?? this.length;
    for (let i = 0; i < id_length; i++) {
      const traceIndex = ids ? ids[i] : i;
      for (let j = this.traceRange[0]; j < this.traceRange[1]; j++) {
        if (isNaN(this.data.data[traceIndex * this.traceLength + j])) {
          throw new Error("isNaN");
        }
        mean[j] += this.data.data[traceIndex * this.traceLength + j];
      }
    }
    for (let j = this.traceRange[0]; j < this.traceRange[1]; j++) {
      mean[j] /= id_length;
    }
    return new Trace(mean, this.traceRange[0]);
  }

  public calculateDiff(
    guess: number,
    textin: Textin,
    byteindex = 0,
    bitnum = 0
  ): DiffRet {
    if (!textin) {
      throw Error("Traces are null!");
    }
    const one_list = [];
    const zero_list = [];

    for (let i = 0; i < this.length; i++) {
      const key = textin.at(i)[byteindex];
      const hypothetical_leakage = aesInternal(guess, key);

      if (hypothetical_leakage & (1 << bitnum)) {
        one_list.push(i);
      } else {
        zero_list.push(i);
      }
    }
    const one_avg = this.meanOfIds(one_list);
    const zero_avg = this.meanOfIds(zero_list);
    const diff = one_avg.subtract(zero_avg).abs();
    return {
      one_avg,
      zero_avg,
      diff,
      guess,
    };
  }

  public getStdDev(ids: number[] | undefined = undefined) {
    const mean = this.meanOfIds();
    const variance = new Float64Array(this.traceLength);
    for (let i = 0; i < (ids?.length ?? this.length); i++) {
      const traceIndex = ids ? ids[i] : i;
      for (let j = this.traceRange[0]; j < this.traceRange[1]; j++) {
        if (isNaN(this.data.data[traceIndex * this.traceLength + j])) {
          throw new Error("isNaN");
        }
        variance[j] +=
          (this.data.data[traceIndex * this.traceLength + j] - mean.at(j)!) **
          2;
      }
    }
    for (let j = this.traceRange[0]; j < this.traceRange[1]; j++) {
      variance[j] = Math.sqrt(variance[j]);
    }
    return new Trace(
      variance.slice(this.traceRange[0], this.traceRange[1]),
      this.traceRange[0]
    );
  }
  public getVariance(ids: number[] | undefined = undefined) {
    const mean = this.meanOfIds();
    const variance = new Float64Array(this.traceLength);
    for (let i = 0; i < (ids?.length ?? this.length); i++) {
      const traceIndex = ids ? ids[i] : i;
      for (let j = this.traceRange[0]; j < this.traceRange[1]; j++) {
        if (isNaN(this.data.data[traceIndex * this.traceLength + j])) {
          throw new Error("isNaN");
        }
        variance[j] +=
          (this.data.data[traceIndex * this.traceLength + j] - mean.at(j)!) **
          2;
      }
    }
    for (let j = this.traceRange[0]; j < this.traceRange[1]; j++) {
      variance[j] /= ids?.length ?? this.length;
    }
    return new Trace(
      variance.slice(this.traceRange[0], this.traceRange[1]),
      this.traceRange[0]
    );
  }

  toArray(): Trace[] {
    const Tarray = [];
    console.log("Start copy!");
    for (let i = 0; i < this.length; i++) {
      Tarray.push(this.getTrace(i));
    }
    console.log("End copy!");
    return Tarray;
  }
}

export function cov(X: Trace, Y: Trace) {
  let sum = 0;
  if (X.length !== Y.length) {
    throw new Error("Non matching length of X, Y");
  }
  for (let i = 0; i < X.length; i++) {
    sum += (Y.at(i)! - Y.mean) * (X.at(i)! - X.mean);
  }
  return sum;
}

export class Trace {
  constructor(
    public readonly trace_data: Float64Array,
    public minOffset: number = 0,
    public opts: { bucketSize: number } = { bucketSize: 1 }
  ) {}

  public graphSettings = {
    label: "Trace",
  };

  get length(): number {
    return this.trace_data.length;
  }

  private _mean: number = NaN;
  private _max: number = NaN;

  get mean(): number {
    if (isNaN(this._mean)) {
      let sum = 0;
      let i = 0;
      for (i = 0; i < this.trace_data.length; i++) {
        const retval = this.trace_data.at(i);
        if (retval === undefined || isNaN(retval)) {
          break;
        }
        sum += this.trace_data.at(i)!;
      }

      this._mean = sum / i;
    }
    if (isNaN(this._mean)) {
      throw new Error("mean is NaN");
    }
    return this._mean;
  }

  get max(): number {
    if (isNaN(this._max)) {
      let max = 0;
      for (let i = 0; i < this.trace_data.length; i++) {
        if (max < this.trace_data.at(i)!) {
          max = this.trace_data.at(i)!;
        }
      }
      this._max = max;
    }
    return this._max;
  }

  get argMax(): number {
    let max = 0;
    let argMax = 0;
    for (let i = 0; i < this.trace_data.length; i++) {
      if (max < this.trace_data.at(i)!) {
        max = this.trace_data.at(i)!;
        argMax = i;
      }
    }
    this._max = max;
    return argMax;
  }

  get rawData(): Float64Array {
    return this.trace_data;
  }

  get graphData(): { x: number; y: number }[] {
    const arr = new Array<{ x: number; y: number }>();
    for (let x = 0; x < this.length; x++) {
      arr[x] = {
        x: x * this.opts.bucketSize + this.minOffset,
        y: this.rawData[x],
      };
    }
    return arr;
  }

  at(i: number) {
    return this.trace_data.at(i);
  }

  subtract(a: Trace): Trace {
    let arr = new Float64Array(this.length);
    for (let i = 0; i < this.length; i++) {
      arr[i] = this.trace_data[i] - a.at(i)!;
    }
    return new Trace(arr, this.minOffset);
  }

  abs(): Trace {
    let data = this.rawData.slice();
    for (let i = 0; i < data.length; i++) {
      data[i] = Math.abs(data[i]);
    }
    return new Trace(data, this.minOffset);
  }

  bucket(bucketSize: number): Trace {
    let arr = new Float64Array(this.rawData.length / bucketSize);
    for (let i = 0; i < this.rawData.length; i = i + bucketSize) {
      for (let j = 0; j < bucketSize; j++) {
        arr[i] += this.at(j + i)!;
      }
    }
    return new Trace(arr, this.minOffset, { bucketSize });
  }
}

export interface DiffRet {
  one_avg: Trace;
  zero_avg: Trace;
  diff: Trace;
  guess: number;
}
