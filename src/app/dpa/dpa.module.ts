import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DpaMainComponent } from "./pages/dpa-main/dpa-main.component";
import { SharedModule } from "../shared/shared.module";
import { ComponentsModule } from "../components/components.module";
import { NgChartsModule } from "ng2-charts";
import { ScaTraceGraphComponent } from "./_components/sca-trace-graph/sca-trace-graph.component";
import { ReactiveFormsModule } from "@angular/forms";
import { BarChartRankingComponent } from "./_components/bar-chart-ranking/bar-chart-ranking.component";
import { NgxSliderModule } from "@angular-slider/ngx-slider";
import { KeyVarGraphComponent } from './_components/key-var-graph/key-var-graph.component';
import { CpaComponent } from './pages/cpa/cpa.component';
import { ScaComparisionTraceComponent } from './_components/sca-comparision-trace/sca-comparision-trace.component';

@NgModule({
  declarations: [
    DpaMainComponent,
    ScaTraceGraphComponent,
    BarChartRankingComponent,
    KeyVarGraphComponent,
    CpaComponent,
    ScaComparisionTraceComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComponentsModule,
    NgChartsModule,
    ReactiveFormsModule,
    ComponentsModule,
    NgxSliderModule,
  ],
})
export class DpaModule {}
