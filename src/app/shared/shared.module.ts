import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormTextFieldComponent } from "./_component/form-text-field/form-text-field.component";
import { ReactiveFormsModule } from "@angular/forms";
import { ButtonComponent } from "./_component/button/button.component";
import { ToggleComponent } from "./_component/toggle/toggle.component";
import { HexNumberPipe } from "./_pipes/hex-number.pipe";

@NgModule({
  declarations: [
    FormTextFieldComponent,
    ButtonComponent,
    ToggleComponent,
    HexNumberPipe,
  ],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [
    FormTextFieldComponent,
    ButtonComponent,
    ToggleComponent,
    HexNumberPipe,
  ],
})
export class SharedModule {}
