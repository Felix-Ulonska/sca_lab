import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "hexNumber",
})
export class HexNumberPipe implements PipeTransform {
  transform(value: number, ...args: unknown[]): unknown {
    const out = value.toString(16);
    if (out.length == 1) {
      return "0" + out;
    } else {
      return out;
    }
  }
}
