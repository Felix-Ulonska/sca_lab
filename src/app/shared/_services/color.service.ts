import { Injectable } from "@angular/core";

// @ts-ignore
import * as tailwindConfig from "../../../../tailwind.config.js";
import resolveConfig from "tailwindcss/resolveConfig";

@Injectable({
  providedIn: "root",
})
export class ColorService {
  getColors() {
    const fullConfig = resolveConfig(tailwindConfig);
    return fullConfig.theme.colors;
  }
}
