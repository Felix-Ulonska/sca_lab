import { Component, Output } from "@angular/core";
import { ControlValueAccessor } from "@angular/forms";
import { BehaviorSubject } from "rxjs";

@Component({
  selector: "sca-toggle",
  templateUrl: "./toggle.component.html",
  styleUrls: ["./toggle.component.scss"],
})
export class ToggleComponent implements ControlValueAccessor {
  _value = false;
  _onChange: ((arg1: boolean) => void) | null = null;
  _onTouched: ((arg1: boolean) => void) | null = null;

  @Output() onChange = new BehaviorSubject<boolean>(false);

  writeValue(obj: boolean): void {
    this._value = obj;
  }
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  onCheckChange(val: any) {
    const checked = val!.currentTarget!.checked;
    if (this._onChange) this._onChange(checked);
    this.onChange.next(checked);
  }

  setDisabledState?(isDisabled: boolean): void {}
}
