import {
  Component,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Self,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NgControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import { Subject } from "rxjs";

@Component({
  selector: "sca-form-text-field",
  templateUrl: "./form-text-field.component.html",
  styleUrls: ["./form-text-field.component.scss"],
})
export class FormTextFieldComponent
  implements ControlValueAccessor, OnDestroy, OnInit
{
  destroy$ = new Subject<void>();

  @Input() isNumber = false;
  @Input() inputName = "";

  public formControl = new FormControl();
  public onChange = (_newVal: string) => {};
  public onTouch = () => {};
  public ValisNaN = false;

  get errorMsg(): string {
    return this.controlDirective.getError("errorMsg");
  }

  constructor(@Self() public controlDirective: NgControl) {
    this.controlDirective.valueAccessor = this;
  }
  ngOnInit(): void {
    this.formControl.valueChanges.subscribe((newVal) => {
      this.onChange(newVal);
      if (this.isNumber) {
        this.ValisNaN = isNaN(parseInt(newVal));
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  writeValue(obj: any): void {
    this.formControl.setValue(obj);
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
}
