import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "sca-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"],
})
export class ButtonComponent {
  @Input()
  color: "normal" | "alert" | "primary" | "green" = "normal";

  @Input()
  disabled = false;
  @Output() public click: EventEmitter<MouseEvent> = new EventEmitter();

  onButtonClick(event: MouseEvent) {
    event.stopPropagation();
    if (!this.disabled) {
      this.click.emit(event);
    }
  }

  get cssClass() {
    if (this.disabled) {
      return "bg-gray-400 opacity-50 cursor-not-allowed";
    }
    switch (this.color) {
      case "normal":
        return "bg-gray-500 hover:bg-gray-700";
      case "alert":
        return "bg-primary-500 hover:bg-primary-700";
      case "primary":
        return "bg-blue-400 hover:bg-blue-600";
      case "green":
        return "bg-green-400 hover:bg-green-600";
    }
  }
}
