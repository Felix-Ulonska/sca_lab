import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ChartConfiguration, Chart } from "chart.js";
import { BehaviorSubject, firstValueFrom, map, Observable } from "rxjs";
import { Mode, OptsRSA, RsaScaService, Trace } from "./_services/rsa-sca.service";
import annotationPlugin from "chartjs-plugin-annotation";
import { TraceUtilService } from "./_services/trace-util.service";
import { GraphData } from "./components/graph/graph.component";
@Component({
  selector: "sca-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "sca";

  rsaCode = `
s = 1;
for bits d_k, d_{k-1}, ..., d_0 of d:
  if d_i = 1 then
    r = s * M mod m
  else
    r = s
  s = r * r mod N 
return r
  `;

  traces$ = new BehaviorSubject<Trace[]>([]);
  bits$ = new BehaviorSubject<boolean[]>([]);
  key$ = new BehaviorSubject<number>(0);
  correctBits$ = this.key$.pipe(map((key) => this.rsaScaService.dec2bin(key)));

  graphData$ = new BehaviorSubject<GraphData[]>([]);

  public formGroup = new FormGroup({
    mod: new FormControl(10),
    tMod: new FormControl(19),
    key: new FormControl(27),
    numberTraces: new FormControl(100000),
  });

  tracesCount$ = this.traces$.pipe(
    map((traces) => this.traceUtilService.groupTracesByTime(traces))
  );

  public opts$ = new BehaviorSubject<OptsRSA>({ mod: 0, tMod: 0, mode: Mode.hammig });

  constructor(
    private rsaScaService: RsaScaService,
    private traceUtilService: TraceUtilService
  ) {
    Chart.register(annotationPlugin);
  }

  public count(traces: Trace[]) {
    return traces.reduce(
      (prev, trace) => ({ ...prev, [trace.t]: prev[trace.t] + 1 || 1 }),
      {} as { [index: number]: number }
    );
  }
}
