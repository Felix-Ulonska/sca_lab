import { HttpClient } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";

import { RsaScaService } from "./rsa-sca.service";

describe("RsaScaService", () => {
  let service: RsaScaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: null,
        },
      ],
    });
    service = TestBed.inject(RsaScaService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("dec2bin workds", () => {
    expect(service).toBeTruthy();
    expect(service.dec2bin(4)).toStrictEqual([true, false, false]);
    expect(service.dec2bin(1)).toStrictEqual([true]);
    expect(service.dec2bin(0)).toStrictEqual([false]);
    expect(service.dec2bin(5)).toStrictEqual([true, false, true]);
  });

  it("genTrace and genTraces works", () => {
    expect(service.genTrace(1, 1, { mod: 5, tMod: 15 })).toStrictEqual({
      x: 1,
      signX: 1,
      t: 2,
      s: 1,
      s_end: 1,
    });
    expect(service.genTrace(10, 0, { mod: 5, tMod: 15 })).toStrictEqual({
      x: 10,
      s: 1,
      s_end: 1,
      signX: 1,
      t: 2,
    });
    expect(service.genTrace(10, 2, { mod: 5, tMod: 15 })).toStrictEqual({
      x: 10,
      s: 1,
      s_end: 10000,
      signX: 100,
      t: 4,
    });
    expect(service.genTraces(100)).toHaveLength(100);
  });

  it("Split trace works", () => {
    const opts = { mod: 100, tMod: 100 };
    expect(
      service.splitTraceByBitGuess([service.genTrace(10, 3, opts)], opts)
    ).toStrictEqual({
      tracesBit0: [
        { x: 10, signX: 1000, t: 101, s: 1, pred_t: 101, s_end: 2549 },
      ],
      tracesBit1: [
        { x: 10, signX: 1000, t: 101, s: 100, s_end: 2549, pred_t: 0 },
      ],
    });
  });

  it("step works", () => {
    expect(service.step(service.genTraces(5, 3)).bit).toBeTruthy();
    expect(service.step(service.genTraces(5, 4)).bit).toBeFalsy();
    expect(service.step(service.genTraces(1000, 4)).bit).toBeFalsy();

    const key = 13;
    const opts = { tMod: 100, mod: 10 };
    let traces = service.genTraces(100000, key, opts);
    const bits = service.dec2bin(key);
    for (let i = 0; i < bits.length; i++) {
      const currBits = parseInt(
        bits
          .slice(0, i + 1)
          .map((f) => (f ? 1 : 0))
          .join(""),
        2
      );

      const debug_traces = service.genTraces(100000, currBits, opts);

      const newStep = service.step(traces, opts);
      expect(newStep.bit).toStrictEqual(bits[i]);

      // verify that traces are calced correctly
      expect(
        debug_traces.slice(1, 1000).map((trace) => trace.s_end)
      ).toStrictEqual(newStep.traces.slice(1, 1000).map((trace) => trace.s));

      traces = newStep.traces;
    }
  });

  it("variance works", () => {
    expect(service.var([1, 1, 1])).toStrictEqual(0);
    expect(service.var([1, 2])).toStrictEqual(0.5);
    expect(service.var([1, 2, 3, 4, 5])).toStrictEqual(2.5);
  });

  it("mu works", () => {
    expect(service.mu([1, 1, 1])).toStrictEqual(1);
    expect(service.mu([1, 2])).toStrictEqual(1.5);
    expect(service.mu([1, 2, 3, 4, 5])).toStrictEqual(3);
  });
});
