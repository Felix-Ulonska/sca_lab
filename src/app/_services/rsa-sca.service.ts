import { Injectable } from "@angular/core";

export interface Trace {
  x: number;
  signX: number;
  t: number;
  pred_t?: number;
  s: number;
  s_end: number;
}

export enum Mode {
  easy = "easy",
  hammig = "hammig",
}

export interface OptsRSA {
  mod: number;
  tMod: number;
  mode: Mode;
}

export interface MetadataTrace {
  var: number;
  mu: number;
}

@Injectable({
  providedIn: "root",
})
export class RsaScaService {
  // Thanks to https://stackoverflow.com/a/16155417
  public dec2bin(dec: number): boolean[] {
    return (dec >>> 0)
      .toString(2)
      .split("")
      .map((bit) => !!Number(bit));
  }

  private N = 10961;

  hammigDistance(x: number, y: number) {
    return this.dec2bin(x ^ y)
      .map((val) => (val ? 1 : 0))
      .reduce<number>((prev, curr) => prev + curr, 0);
  }

  noiseGenerator() {
    let noise: number[] = [];
    for (let i = 0; i < 1000; i++) {
      noise.push(Math.random() * 2 + 0.5);
    }
    let i = 0;
    return () => {
      return 1;
    };
  }

  genTrace(
    M: number,
    d: number,
    opts = { mod: 5, tMod: 10, mode: Mode.easy }
  ): Trace {
    let time = 0;
    const bits = this.dec2bin(d);
    let s = 1;
    let r = 0;
    bits.forEach((d_i) => {
      if (d_i) {
        r = (s * M) % this.N;
        if (opts.mode === Mode.hammig) {
          time += this.hammigDistance(s, M);
        }
      } else {
        r = s;
      }
      s = (r * r) % this.N;
      if (opts.mode === Mode.easy) {
        time += r % opts.mod === 0 ? opts.tMod : 1;
      }
      time++;
    });
    return {
      x: M,
      signX: r,
      t: time,
      s: 1,
      s_end: s,
    };
  }

  genTraces(
    numberTraces = 1000,
    d = 3,
    opts: OptsRSA = { mod: 5, tMod: 10, mode: Mode.hammig }
  ): Trace[] {
    opts.mode = opts.mode ?? Mode.hammig;
    let traces: Trace[] = [];
    for (let i = 1; i <= numberTraces; i++) {
      traces.push(
        this.genTrace(i, d, opts ?? { mod: 5, tMod: 10, mode: Mode.easy })
      );
    }
    return traces;
  }

  mu(x: number[]): number {
    return x.reduce((prev, curr) => prev + curr, 0) / x.length;
  }

  var(x: number[]): number {
    const mu = this.mu(x);
    const n = x.length;
    return (1 / (n - 1)) * x.reduce((prev, curr) => prev + (curr - mu) ** 2, 0);
  }

  splitTraceByBitGuess(
    traces: Trace[],
    opts = { mod: 5, tMod: 10, mode: Mode.easy }
  ): {
    tracesBit1: Trace[];
    tracesBit0: Trace[];
  } {
    const tracesBit1 = traces.map((trace) => {
      let s = trace.s;
      let r = (s * trace.x) % this.N;
      let t = trace.t - this.hammigDistance(s, trace.x) - 1;
      s = (r * r) % this.N;
      if (opts.mode !== Mode.hammig) {
        t = trace.t - (r % opts.mod === 0 ? opts.tMod + 1 : 2);
      }
      return { ...trace, s, t };
    });

    const tracesBit0 = traces.map((trace) => {
      let s = trace.s;
      let r = s;
      s = (r * r) % this.N;
      let t = 0;
      if (opts.mode === Mode.hammig) {
        t = trace.t - 1;
      } else {
        t = trace.t - (r % opts.mod === 0 ? opts.tMod + 1 : 2);
      }

      return { ...trace, s, t };
    });
    return {
      tracesBit0,
      tracesBit1,
    };
  }

  addMetadataToTrace(traces: Trace[]) {
    return {
      var: this.var(traces.map((trace) => trace.pred_t ?? trace.t)),
      mu: this.mu(traces.map((trace) => trace.pred_t ?? trace.t)),
      traces: traces,
    };
  }

  step(
    traces: Trace[],
    opts = { mod: 5, tMod: 10, mode: Mode.easy }
  ): { traces: Trace[]; bit: boolean } {
    const { tracesBit0, tracesBit1 } = this.splitTraceByBitGuess(traces, opts);

    const var_minus_uj = this.var(tracesBit0.map((trace) => trace.pred_t!));
    const var_minus_vj_xy = this.var(tracesBit1.map((trace) => trace.pred_t!));

    if (var_minus_uj > var_minus_vj_xy) {
      return { traces: tracesBit1, bit: true };
    } else {
      return { traces: tracesBit0, bit: false };
    }
  }

  //parseRSAFile(): Promise<Trace[]> {
  //  return firstValueFrom(
  //    this.http.get("assets/data.txt", { responseType: "text" }).pipe(
  //      map((respText) =>
  //        respText
  //          .split("\n")
  //          .filter((line) => line.length > 3)
  //          .map((line) => line.split(" "))
  //          .map((line) => ({
  //            x: parseInt(line[0]),
  //            signX: parseInt(line[1].replace("(", "")),
  //            t: parseInt(line[2].replace(")", "")),
  //            s: 1,
  //          }))
  //      )
  //    )
  //  );
  //}
}
