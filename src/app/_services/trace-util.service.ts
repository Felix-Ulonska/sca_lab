import { Injectable } from "@angular/core";
import { Trace } from "./rsa-sca.service";

@Injectable({
  providedIn: "root",
})
export class TraceUtilService {
  constructor() {}

  groupTracesByTime(traces: Trace[]): { [index: number]: number } {
    return traces.reduce(
      (prev, trace) => ({
        ...prev,
        [trace.pred_t ?? trace.t!]: prev[trace.pred_t ?? trace.t!] + 1 || 1,
      }),
      {} as { [index: number]: number }
    );
  }
}
