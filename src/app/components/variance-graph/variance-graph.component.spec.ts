import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VarianceGraphComponent } from './variance-graph.component';

describe('VarianceGraphComponent', () => {
  let component: VarianceGraphComponent;
  let fixture: ComponentFixture<VarianceGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VarianceGraphComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VarianceGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
