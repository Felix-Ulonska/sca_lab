import { Component, Input } from "@angular/core";
import { ChartConfiguration } from "chart.js";
import { BehaviorSubject, filter, map, Observable } from "rxjs";

export interface GraphVarianceData {
  label: string;
  color: string;
  dataPts: { x: number; y: number }[];
}

@Component({
  selector: "sca-variance-graph",
  templateUrl: "./variance-graph.component.html",
  styleUrls: ["./variance-graph.component.scss"],
})
export class VarianceGraphComponent {
  public data$ = new BehaviorSubject<GraphVarianceData[]>([]);
  public maxX$ = new BehaviorSubject<number>(0);

  private getLabels(n: number) {
    let labels: string[] = [];
    for (let i = 0; i < n; i++) {
      labels.push(`${i}`);
    }
    return labels;
  }

  public lineChartDatasets$: Observable<ChartConfiguration<"scatter">["data"]> =
    this.data$.pipe(
      filter((traces) => !!traces),
      map((data) => {
        return {
          labels: this.getLabels(data[0].dataPts.length + 1),
          datasets: data.map((dataPt) => {
            return {
              data: dataPt.dataPts,
              label: dataPt.label,
              pointRadius: 5,
              backgroundColor: dataPt.color,
            };
          }),
        };
      })
    );

  public lineChartOptions$: Observable<
    ChartConfiguration<"scatter">["options"]
  > = this.maxX$.pipe(
    map((maxX) => ({
      scales: {
        y: {
          title: { text: "Varianz", display: true, font: { size: 18 } },
          min: 0,
        },
        x: {
          title: { text: "Geratenes Bit", display: true, font: { size: 18 } },
          max: maxX,
        },
      },
      animation: false,
      responsive: true,
      maintainAspectRatio: false,
    }))
  );

  @Input() data: GraphVarianceData[] = [];
  @Input() maxX: number = 0;

  ngOnChanges(changes: any) {
    if (changes.data) {
      const data = changes.data.currentValue;
      this.data$.next(data);
    }
    if (changes.maxX) {
      this.maxX$.next(changes.maxX.currentValue);
    }
  }
}
