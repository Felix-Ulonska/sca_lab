import { Component, Input } from "@angular/core";

@Component({
  selector: "sca-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"],
})
export class CardComponent {
  @Input()
  color: "default" | "green" | "red" = "default";
}
