import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CardComponent } from "./card/card.component";
import { GraphComponent } from "./graph/graph.component";
import { NgChartsModule } from "ng2-charts";
import { SharedModule } from "../shared/shared.module";
import { VarianceGraphComponent } from "./variance-graph/variance-graph.component";
import { HeaderComponent } from "./header/header.component";
import { CollapsableCardComponent } from "./collapsable-card/collapsable-card.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    CardComponent,
    GraphComponent,
    VarianceGraphComponent,
    HeaderComponent,
    CollapsableCardComponent,
  ],
  imports: [
    CommonModule,
    NgChartsModule,
    SharedModule,
    BrowserAnimationsModule,
  ],
  exports: [
    CardComponent,
    GraphComponent,
    VarianceGraphComponent,
    HeaderComponent,
    CollapsableCardComponent,
  ],
})
export class ComponentsModule {}
