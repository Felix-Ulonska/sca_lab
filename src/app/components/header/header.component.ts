import { Component, Input } from "@angular/core";

@Component({
  selector: "sca-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent {
  @Input() title: string = "";
  menuShown = false;
}
