import { Component, Input, OnChanges } from "@angular/core";
import { ChartConfiguration } from "chart.js";
import { BehaviorSubject, filter, map, Observable, tap } from "rxjs";
import { MetadataTrace, Trace } from "../../_services/rsa-sca.service";
import { TraceUtilService } from "../../_services/trace-util.service";

@Component({
  selector: "sca-graph",
  templateUrl: "./graph.component.html",
  styleUrls: ["./graph.component.scss"],
})
export class GraphComponent implements OnChanges {
  public data$ = new BehaviorSubject<GraphData[]>([]);

  public scatterChartDatasets$: Observable<
    ChartConfiguration["data"]["datasets"]
  > = this.data$.pipe(
    filter((traces) => !!traces),
    map((data) => {
      return data.map((dataPt) => {
        const tracesCount = this.traceUtilService.groupTracesByTime(
          dataPt.traces
        );
        return {
          type: "line",
          data: Object.keys(tracesCount)
            .map((traceCount) => ({
              x: Number(traceCount),
              y: tracesCount[Number(traceCount)],
            }))
            .sort((a, b) => {
              if (a.x < b.x) return -1;
              if (a.x > b.x) return 1;
              return 0;
            }),
          label: dataPt.label,
          pointRadius: 5,
          backgroundColor: dataPt.color[900],
          color: dataPt.color[900],
          pointBackgroundColor: dataPt.color[600] + (dataPt.opacity ?? "DD"),
          borderColor: dataPt.color[600] + (dataPt.opacity ?? "DD"),
          fillColor: dataPt.color[600],
          pointStrokeColor: dataPt.color[600],
          //pointStyle: dataPt.pointStyle ?? "circle",
          hoverBorderColor: dataPt.color[800],
          cubicInterpolationMode: "monotone",
        };
      });
    })
  );

  public scatterChartOptions$: BehaviorSubject<ChartConfiguration["options"]> =
    new BehaviorSubject<ChartConfiguration["options"]>({
      scales: {
        y: {
          title: {
            text: "Anzahl der Traces pro Restzeit X",
            display: true,
            font: {
              size: 18,
            },
          },
          display: true,
        },
        x: {
          title: {
            text: "Restzeit",
            display: true,
            font: {
              size: 18,
            },
          },
          display: true,
          min: -10,
        },
      },
      plugins: {
        legend: {
          labels: {
            filter: (item: any) => {
              console.log(item);
              return !(item.text as string).includes("HIDE");
            },
            font: {
              size: 18,
            },
          },
        },
      },
      animation: false,
      responsive: true,
      maintainAspectRatio: false,
    });

  @Input() data: GraphData[] = [];

  constructor(private traceUtilService: TraceUtilService) {}

  ngOnChanges(changes: any) {
    if (changes.data) {
      const data = changes.data.currentValue;
      this.data$.next(data);
    }
  }
}

export interface GraphData {
  traces: Trace[];
  color: {
    100: string;
    200: string;
    300: string;
    400: string;
    500: string;
    600: string;
    700: string;
    800: string;
    900: string;
  };
  label: string;
  metadata: MetadataTrace;
  pointStyle?: string;
  pointRadius?: number;
  opacity?: string;
  borderWidth?: number;
}
