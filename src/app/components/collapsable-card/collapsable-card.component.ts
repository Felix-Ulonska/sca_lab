import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { Component } from "@angular/core";

@Component({
  selector: "sca-collapsable-card",
  templateUrl: "./collapsable-card.component.html",
  styleUrls: ["./collapsable-card.component.scss"],
  animations: [
    trigger("openClose", [
      state("open", style({})),
      state("closed", style({ height: 0 })),
      transition("open => closed", [animate("0.3s")]),
      transition("closed => open", [animate("0.3s")]),
    ]),
    trigger("openCloseDown", [
      state("open", style({ transform: "rotate(0.5turn)" })),
      state("closed", style({ transform: "rotate(0)" })),
      transition("open => closed", [animate("0.3s")]),
      transition("closed => open", [animate("0.3s")]),
    ]),
  ],
})
export class CollapsableCardComponent {
  isOpen = true;
  toggleCard() {
    console.log("Toggle!");
    this.isOpen = !this.isOpen;
  }

  openCard() {
    this.isOpen = true;
  }
}
