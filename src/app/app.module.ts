import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgChartsModule } from "ng2-charts";
import { ComponentsModule } from "./components/components.module";
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "./shared/shared.module";
import { FormGroup, ReactiveFormsModule } from "@angular/forms";
import { RsaTimingAttackModule } from "./rsa-timing-attack/rsa-timing-attack.module";
import { PaddingOracleModule } from "./padding-oracle/padding-oracle.module";
import { MathjaxModule } from "mathjax-angular";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgChartsModule,
    ComponentsModule,
    NgChartsModule,
    HttpClientModule,
    SharedModule,
    ReactiveFormsModule,
    RsaTimingAttackModule,
    PaddingOracleModule,
    MathjaxModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
