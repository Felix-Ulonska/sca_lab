import { Injectable } from "@angular/core";

export interface PaddingOracleOpts {
  blockSize: number;
  blockCount: number;
}

@Injectable({
  providedIn: "root",
})
export class OracleService {
  constructor() {}
}
