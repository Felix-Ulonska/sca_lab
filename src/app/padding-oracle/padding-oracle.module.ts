import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PaddingMainComponent } from "./_components/padding-main/padding-main.component";
import { SharedModule } from "../shared/shared.module";
import { ComponentsModule } from "../components/components.module";
import { BlockInputComponent } from "./_components/block-input/block-input.component";
import { ReactiveFormsModule } from "@angular/forms";
import { BlockOutputComponent } from "./_components/block-output/block-output.component";
import { PaddingOracleExplanationCardComponent } from "./_components/padding-oracle-explanation-card/padding-oracle-explanation-card.component";
import { MathjaxModule } from "mathjax-angular";

@NgModule({
  declarations: [
    PaddingMainComponent,
    BlockInputComponent,
    BlockOutputComponent,
    PaddingOracleExplanationCardComponent,
  ],
  exports: [PaddingMainComponent],
  imports: [
    CommonModule,
    MathjaxModule,
    SharedModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
})
export class PaddingOracleModule {}
