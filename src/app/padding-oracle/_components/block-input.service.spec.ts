import { TestBed } from '@angular/core/testing';

import { BlockInputService } from './block-input.service';

describe('BlockInputService', () => {
  let service: BlockInputService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlockInputService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
