import { Component, Output } from "@angular/core";
import { Subject } from "rxjs";

@Component({
  selector: "sca-padding-oracle-explanation-card",
  templateUrl: "./padding-oracle-explanation-card.component.html",
  styleUrls: ["./padding-oracle-explanation-card.component.scss"],
})
export class PaddingOracleExplanationCardComponent {
  @Output() onChange = new Subject<boolean>();
}
