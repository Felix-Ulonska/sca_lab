import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaddingOracleExplanationCardComponent } from './padding-oracle-explanation-card.component';

describe('PaddingOracleExplanationCardComponent', () => {
  let component: PaddingOracleExplanationCardComponent;
  let fixture: ComponentFixture<PaddingOracleExplanationCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaddingOracleExplanationCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaddingOracleExplanationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
