import { Component, Input } from "@angular/core";
import { PADDING_STATUS } from "../../padding-oracle";

@Component({
  selector: "sca-block-output",
  templateUrl: "./block-output.component.html",
  styleUrls: ["./block-output.component.scss"],
})
export class BlockOutputComponent {
  @Input() cleartext: number[] = [];
  @Input() paddingStatus: PADDING_STATUS[] | null = null;
  @Input() hideText = false;
  PADDING_STATUS = PADDING_STATUS;
}
