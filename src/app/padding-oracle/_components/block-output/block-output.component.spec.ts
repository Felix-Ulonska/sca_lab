import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockOutputComponent } from './block-output.component';

describe('BlockOutputComponent', () => {
  let component: BlockOutputComponent;
  let fixture: ComponentFixture<BlockOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlockOutputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BlockOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
