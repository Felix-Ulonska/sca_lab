import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import {
  BehaviorSubject,
  combineLatest,
  map,
  merge,
  of,
  startWith,
  tap,
} from "rxjs";
import { PaddingOracle, PADDING_STATUS } from "../../padding-oracle";

@Component({
  selector: "sca-padding-main",
  templateUrl: "./padding-main.component.html",
  styleUrls: ["./padding-main.component.scss"],
})
export class PaddingMainComponent implements OnInit {
  initCleartext = [
    [0xaa, 0x12, 0x42, 0xa0, 0x53, 0x2, 0x53, 0],
    [0, 0xaa, 0x12, 0x07, 0x08, 0x12, 0x31, 0x1],
  ];
  attack = new PaddingOracle({
    key: [0xff, 0xac, 21, 12, 0x42, 0x53, 0x12, 0x32],
    IV: [1, 2, 3, 4, 5, 6, 7, 8],
    cleartext: this.initCleartext,
  });
  form = new FormGroup({
    IV: new FormControl([0x21, 0x32, 0x1, 0xff, 0x12, 0x2, 0xaa, 0xcc]),
    "0": new FormControl([0x12, 0x42, 0xf, 0x92, 0x12, 0x21, 0xaa, 0]),
    "1": new FormControl([0, 0, 0, 0, 0, 0, 0, 0]),
  });
  decryption$ = this.form.valueChanges.pipe(
    startWith(null),
    map((val) => val ?? this.form.value),
    map((val) =>
      this.attack.decryption([val[0], val[1]] as number[][], val.IV!)
    )
  );

  decrytpionStatusString$ = this.decryption$.pipe(
    map((val) => {
      switch (val.status) {
        case "ok":
          return "Decryption erfolgreich!";
        case "bad_padding":
          return "Padding ist fehlerhaft!";
        case "bad_decryption":
          return "Padding ist richtig, aber Klartext ist fehlerhaft";
      }
    })
  );

  hideCleartext$ = new BehaviorSubject(false);

  cleartext$ = this.decryption$.pipe(map((decryption) => decryption.cleartext));
  xor_intermediate$ = this.decryption$.pipe(
    map((decryption) => decryption.xor_intermediate)
  );
  paddingStatus$ = this.cleartext$.pipe(
    map((cleartext) =>
      this.attack.checkPadding(cleartext[cleartext.length - 1])
    )
  );

  PADDING_STATUS = PADDING_STATUS;

  status$ = this.decryption$.pipe(map((decryption) => decryption.status));

  ngOnInit(): void {
    const enc = this.attack.encryption(this.initCleartext);
    console.log("env", enc);
    this.form.setValue(
      {
        IV: this.attack.IV,
        0: this.attack.encryption(this.initCleartext)[0],
        1: this.attack.encryption(this.initCleartext)[1],
      },
      { emitEvent: true }
    );
  }
}
