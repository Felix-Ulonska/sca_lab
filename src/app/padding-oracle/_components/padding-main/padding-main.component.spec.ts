import { ComponentFixture, TestBed } from "@angular/core/testing";

import { PaddingMainComponent } from "./padding-main.component";

describe("PaddingMainComponent", () => {
  let component: PaddingMainComponent;
  let fixture: ComponentFixture<PaddingMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaddingMainComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PaddingMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
