import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockInputComponent } from './block-input.component';

describe('BlockInputComponent', () => {
  let component: BlockInputComponent;
  let fixture: ComponentFixture<BlockInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlockInputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BlockInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
