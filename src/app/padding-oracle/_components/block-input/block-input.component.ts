import { Component, forwardRef, Input, OnInit } from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  PatternValidator,
  Validators,
} from "@angular/forms";

@Component({
  selector: "sca-block-input",
  templateUrl: "./block-input.component.html",
  styleUrls: ["./block-input.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BlockInputComponent),
      multi: true,
    },
  ],
})
export class BlockInputComponent implements OnInit, ControlValueAccessor {
  @Input() blockSize: number = 8;
  @Input() validPadding: boolean = false;
  public formGroup = new FormGroup({});
  _onChange = (val: number[]) => undefined;
  _onTouch = (val: number[]) => undefined;
  _disabled = false;

  get controlKeys() {
    return Object.keys(this.formGroup.controls);
  }

  ngOnInit() {
    for (let i = 0; i < this.blockSize; i++) {
      this.formGroup.addControl(
        `${i}`,
        new FormControl<string>("00", [
          Validators.pattern(new RegExp("([0-9]|[A-F])?([0-9]|[A-F])", "i")),
          Validators.maxLength(2),
        ])
      );
    }
    this.formGroup.valueChanges.subscribe((newVal) => {
      this._onChange(
        (Object.values(newVal) as string[]).map((val) => parseInt(val, 16))
      );
    });
  }

  leadingZero(hexNum: string) {
    return hexNum.length == 1 ? "0" + hexNum : hexNum;
  }

  writeValue(arr: number[]): void {
    this.controlKeys.forEach((val, i) =>
      this.formGroup.get(val)!.setValue(this.leadingZero(arr[i].toString(16)))
    );
  }
  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  sleep(sec: number): Promise<void> {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        resolve();
      }, sec);
    });
  }

  async fixPadding(place: string) {
    for (let i = 0; i < 255; i++) {
      if (this.validPadding) {
        break;
      }
      const oldVal: number = parseInt(
        Object.values(this.formGroup.value)[parseInt(place, 10)] as string,
        16
      );
      this.formGroup.get(place)?.setValue(((oldVal + 1) % 255).toString(16));
      await this.sleep(100);
    }
  }
}
