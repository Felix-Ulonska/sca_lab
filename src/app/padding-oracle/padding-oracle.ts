import { BehaviorSubject, last } from "rxjs";

export interface PaddingOracleOpts {
  key: number[];
  IV: number[];
  cleartext: number[][];
}

export enum DecryptionStatus {
  OK = 0,
  BAD_PADDING = 1,
  BAD_DECRYPTION = 2,
}

export class PaddingOracle {
  state$ = new BehaviorSubject<void>(undefined);
  key: number[] = [12, 2, 0xca, 0x12, 0x23, 0x42, 0xff];
  cleartext: number[][] = [
    [1, 2, 3, 4, 5, 6, 7, 8],
    [9, 10, 11, 12, 13, 14, 15, 16],
  ];
  IV = [0xc, 1, 45, 0, 0, 0, 0, 0];

  constructor(private opts: PaddingOracleOpts) {
    this.key = opts.key;
    this.IV = opts.IV;
    this.cleartext = opts.cleartext;
  }

  public encryption(cleartext: number[][]): number[][] {
    console.log("encrypting", cleartext);
    let curr = this.IV;
    let output = [];
    for (let i = 0; i < cleartext.length; i++) {
      const beforeEnc = this.blockwiseXor(cleartext[i], curr);
      const encryption = this.encryptionBlock(beforeEnc);
      curr = encryption;
      output.push(encryption);
    }
    return output;
  }

  public decryption(
    ciphertext: number[][],
    IV = this.IV
  ): {
    cleartext: number[][];
    xor_intermediate: number[][];
    status: "ok" | "bad_decryption" | "bad_padding";
  } {
    let status: "ok" | "bad_decryption" | "bad_padding" = "ok";
    let curr = IV;
    let output = [];
    let xor_intermediate: number[][] = [];
    for (let i = 0; i < ciphertext.length; i++) {
      const decryption = this.decryptionBlock(ciphertext[i]);
      xor_intermediate.push(decryption);
      const afterEnc = this.blockwiseXor(decryption, curr);
      curr = ciphertext[i];
      output.push(afterEnc);
    }
    if (
      !output.every((val, i) =>
        val.every((entry, j) => entry === this.cleartext[i][j])
      )
    ) {
      status = "bad_decryption";
    }

    const last_block = output[output.length - 1];
    const paddingSize = last_block[last_block.length - 1];
    for (
      let i = last_block.length - 1;
      i > 0 && i >= last_block.length - paddingSize;
      i--
    ) {
      if (last_block[i] !== paddingSize) {
        status = "bad_padding";
      }
    }

    return {
      cleartext: output,
      xor_intermediate,
      status,
    };
  }

  public checkPadding(block: number[]) {
    const paddingSize = block[block.length - 1];
    let validPadding: PADDING_STATUS[] = [];
    for (let i = block.length - 1; i >= 0; i--) {
      if (block[i] === paddingSize && i > block.length - paddingSize - 1) {
        validPadding.push(PADDING_STATUS.GOOD_PADDING);
      } else if (
        block[i] !== paddingSize &&
        i > block.length - paddingSize - 1
      ) {
        validPadding.push(PADDING_STATUS.BAD_PADDING);
      } else {
        validPadding.push(PADDING_STATUS.NO_PADDING);
      }
    }
    return validPadding.reverse();
  }

  private blockwiseXor(b1: number[], b2: number[]) {
    if (b1.length !== b2.length) {
      throw new Error("NonMatching Block length");
    }

    return b1.map((val, i) => val ^ b2[i]);
  }

  private xor(a: number, b: number) {
    const xor = (arr: number[]) => {
      arr = arr.slice();
      arr[a] ^= arr[b];
      return arr;
    };
    return {
      forward: xor,
      reverse: xor,
    };
  }
  private swap(a: number, b: number) {
    const xor = (arr: number[]) => {
      arr = arr.slice();
      const val = arr[a];
      arr[a] = arr[b];
      arr[b] = val;
      return arr;
    };
    return {
      forward: xor,
      reverse: xor,
    };
  }

  public shift(a: number) {
    if (a < 0) {
      throw new Error("No negative Error!");
    }
    return {
      forward: (arr: number[]) => {
        let binNum = this.toBinNum(arr);
        return this.fromBinNum(
          binNum.slice(binNum.length - a) + binNum.slice(0, binNum.length - a)
        );
      },
      reverse: (arr: number[]) => {
        return this.fromBinNum(
          this.toBinNum(arr).slice(a) + this.toBinNum(arr).slice(0, a)
        );
      },
    };
  }

  private constXor(a: number, b: number) {
    return (arr: number[]) => {
      arr[a] ^= b;
      return arr;
    };
  }

  public toBinNum(arr: number[]): string {
    function bumpLength(str: string) {
      let s = str;
      for (let i = str.length; i < 8; i++) {
        s = "0" + s;
      }
      return s;
    }
    return arr.map((num) => bumpLength(num.toString(2))).join("");
  }

  public fromBinNum(binNum: string): number[] {
    const ret = [];
    for (let i = 0; i < binNum.length; i += 8) {
      ret.push(parseInt(binNum.slice(i, i + 8), 2));
    }
    return ret;
  }

  private ops = [
    this.shift(3),
    this.xor(2, 3),
    this.xor(1, 3),
    this.xor(1, 6),
    this.xor(1, 2),
    this.xor(3, 2),
    this.xor(3, 7),
    this.xor(1, 7),
    this.shift(2),
    this.xor(2, 3),
    this.xor(3, 2),
    this.xor(1, 5),
    this.shift(3),
    this.xor(2, 3),
    this.xor(3, 2),
    this.xor(1, 5),
    this.shift(1),
    this.xor(2, 7),
    this.xor(1, 3),
    this.xor(3, 7),
    this.shift(8),
    this.xor(6, 0),
    this.xor(4, 3),
    this.shift(2),
    this.xor(2, 7),
    this.xor(1, 6),
    this.xor(3, 2),
    this.xor(1, 5),
    this.shift(3),
    this.xor(2, 3),
    this.xor(3, 2),
    this.xor(1, 6),
    this.shift(8),
  ];

  private encryptionBlock(block: number[]): number[] {
    this.ops.forEach((op) => {
      block = op.forward(block);
    });
    block = block.map((val, i) => val ^ this.key[i]);
    return block;
  }

  private decryptionBlock(block: number[]): number[] {
    block = block.slice().map((val, i) => val ^ this.key[i]);
    this.ops
      .slice()
      .reverse()
      .forEach((op) => {
        block = op.reverse(block);
      });
    return block;
  }
}

export enum PADDING_STATUS {
  GOOD_PADDING,
  BAD_PADDING,
  NO_PADDING,
}
