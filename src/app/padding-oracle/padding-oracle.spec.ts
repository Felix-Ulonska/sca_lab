import { TestBed } from "@angular/core/testing";
import { PaddingOracle } from "./padding-oracle";

describe("OracleService", () => {
  beforeEach(() => {});

  it("should be created", () => {
    const po = new PaddingOracle({
      key: [0xff, 0xac, 21, 12, 0x42, 0x53, 0x12, 0x32],
      IV: [1, 2, 3, 4, 5, 6, 7, 8],
      cleartext: [
        [0xaa, 0x12, 0x42, 0xa0, 0x53, 0x2, 0x53, 0],
        [0, 0xaa, 0, 0x31, 0x31, 0x5, 0x2, 0x2],
      ],
    });
  });

  it("BinToNum Works", () => {
    const po = new PaddingOracle({
      key: [0xff, 0xac, 21, 12, 0x42, 0x53, 0x12, 0x32],
      IV: [1, 2, 3, 4, 5, 6, 7, 8],
      cleartext: [
        [0xaa, 0x12, 0x42, 0xa0, 0x53, 0x2, 0x53, 0],
        [0, 0xaa, 0, 0x31, 0x31, 0x5, 0x2, 0x2],
      ],
    });
    expect(po.toBinNum([3])).toHaveLength(8);
    expect(po.toBinNum([5, 3])).toBe("0000010100000011");
    expect(po.fromBinNum(po.toBinNum([5, 3]))).toEqual([5, 3]);
    const myShift = po.shift(1);
    expect(myShift.forward([3])).toEqual([129]);
    expect(myShift.reverse(myShift.forward([20, 0xff, 2, 1]))).toEqual([
      20, 0xff, 2, 1,
    ]);
  });
});
