import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainComponent } from "./_components/main/main.component";
import { SharedModule } from "../shared/shared.module";
import { ComponentsModule } from "../components/components.module";
import { ReactiveFormsModule } from "@angular/forms";
import { HighlightModule, HIGHLIGHT_OPTIONS } from "ngx-highlightjs";
import { KocherExplanationComponent } from "./_components/kocher-explanation/kocher-explanation.component";
import { GuessCardComponent } from "./_components/guess-card/guess-card.component";
import { MathjaxModule } from "mathjax-angular";

@NgModule({
  declarations: [MainComponent, KocherExplanationComponent, GuessCardComponent],
  imports: [
    CommonModule,
    SharedModule,
    ComponentsModule,
    ReactiveFormsModule,
    HighlightModule,
    MathjaxModule.forChild(),
  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        fullLibraryLoader: () => import("highlight.js"),
      },
    },
  ],
  exports: [MainComponent],
})
export class RsaTimingAttackModule {}
