import { Component, Input, Output } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";

@Component({
  selector: "sca-guess-card",
  templateUrl: "./guess-card.component.html",
  styleUrls: ["./guess-card.component.scss"],
})
export class GuessCardComponent {
  @Input() currentGuessedBits: { bit: boolean; correct: boolean }[] = [];
  @Input() keyLength: number = 0;
  @Output() next = new Subject<"0" | "1">();
  @Output() back = new Subject<void>();

  get done(): boolean {
    return this.keyLength == this.currentGuessedBits.length;
  }
}
