import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import {
  BehaviorSubject,
  combineLatest,
  distinct,
  firstValueFrom,
  map,
  Observable,
  tap,
} from "rxjs";
import { Mode, RsaScaService } from "../../../_services/rsa-sca.service";
import { RsaTimingAttackStateService } from "../../_services/rsa-timing-attack-state.service";
import { code, codeHamming } from "./rsaCode";

@Component({
  selector: "sca-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"],
})
export class MainComponent {
  public formGroup = new FormGroup({
    mod: new FormControl<number>(3, Validators.required),
    tMod: new FormControl<number>(19, Validators.required),
    key: new FormControl<number>(9123, Validators.required),
    numberTraces: new FormControl<number>(1000, Validators.required),
  });

  TimingMode = Mode;

  timingMode$ = new BehaviorSubject<Mode>(Mode.easy);

  code$ = this.timingMode$.pipe(
    map((mode) => (mode === Mode.easy ? code : codeHamming))
  );

  varainceEquation = "$$Var(X)=\\sum_{i = 1}^{n}p_i*(x_i-\\mu)^2$$";

  constructor(
    private rsaTimingAttackStateService: RsaTimingAttackStateService,
    private rsaScaService: RsaScaService,
    private router: Router
  ) {}

  get graphData$() {
    return this.rsaTimingAttackStateService.graphData$;
  }

  get lenTraces$() {
    return this.rsaTimingAttackStateService.attackState$.pipe(
      map((state) => state?.traces.length)
    );
  }

  get allGuessed$() {
    return this.rsaTimingAttackStateService.attackState$.pipe(
      map(
        (state) =>
          state!.currentGuessedBits.length >=
          this.rsaScaService.dec2bin(state!.key).length
      )
    );
  }

  get graphVarianceData$() {
    return this.rsaTimingAttackStateService.graphDataVariance$;
  }

  get isAttackRunning$() {
    return this.rsaTimingAttackStateService.isAttackRunning$;
  }

  get currentGuessedBits$(): Observable<
    { bit: boolean; correct: boolean }[] | undefined
  > {
    return this.rsaTimingAttackStateService.attackState$.pipe(
      map((state) =>
        state?.currentGuessedBits.map((bit, index) => ({
          bit,
          correct: this.rsaScaService.dec2bin(state.key)[index] === bit,
        }))
      )
    );
  }

  get correctedBits$() {
    return this.rsaTimingAttackStateService.attackState$.pipe(
      map((state) => this.rsaScaService.dec2bin(state!.key))
    );
  }

  get keyLength$() {
    return this.rsaTimingAttackStateService.attackState$.pipe(
      map((state) => this.rsaScaService.dec2bin(state!.key).length)
    );
  }

  finished$ = combineLatest([this.keyLength$, this.currentGuessedBits$]).pipe(
    tap((val) => {
      console.log(val);
    }),
    map(([keyLength, guessedBits]) => keyLength === guessedBits?.length)
  );

  isCorrect$ = this.currentGuessedBits$.pipe(
    map((guessedBits) => guessedBits?.every((bit) => bit.correct))
  );

  public startHack() {
    const { key, mod, numberTraces, tMod } = this.formGroup.getRawValue();
    this.rsaTimingAttackStateService.initNewAttack({
      key: key!,
      numberTraces: numberTraces!,
      rsaOpts: {
        mod: mod!,
        tMod: tMod!,
        mode: this.timingMode$.value,
      },
    });
  }

  public back() {
    this.rsaTimingAttackStateService.back();
  }

  async next(trace: "0" | "1") {
    if (!(await firstValueFrom(this.allGuessed$))) {
      this.rsaTimingAttackStateService.next(trace);
    }
  }

  switchToHamming() {
    this.timingMode$.next(this.TimingMode.hammig);
  }

  switchToEasy() {
    this.timingMode$.next(this.TimingMode.easy);
  }

  easy_mode_good() {
    this.switchToEasy();
    this.formGroup.setValue({
      key: 9123,
      tMod: 19,
      mod: 3,
      numberTraces: 1000,
    });
  }

  easy_mode_bad() {
    this.switchToEasy();
    this.formGroup.setValue({
      key: 9123,
      tMod: 19,
      mod: 20,
      numberTraces: 45,
    });
  }

  hammig_mode_good() {
    this.switchToHamming();
    this.formGroup.setValue({
      key: 9123,
      tMod: 19,
      mod: 3,
      numberTraces: 1000,
    });
  }

  hammig_mode_bad() {
    this.switchToHamming();
    this.formGroup.setValue({
      key: 9123,
      tMod: 19,
      mod: 10,
      numberTraces: 45,
    });
  }
  to_padding_oracle() {
    this.router.navigate(["/padding"]);
  }
}
