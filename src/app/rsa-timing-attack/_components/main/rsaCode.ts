export const code = `const N = 10961;
function sign(key: number, M: number) {
  let s = 1;
  let r = 0;
  for d_i of bits(key) { // d_i ist das ite Bit
    if (d_i) {
      r = (s * M) % N;
    } else {
      r = s;
    }
    s = (r * r) % N; // time += r % opts.mod === 0 ? opts.tMod : 1;
  }
  return s;
}`;

export const binExpCode = `
function bin_exp(x,b)
  res = 1
  for i = n..0
    res = res^2
    if b_i == 1
      res = res * x
    end-if
  end-for
  return res
end-function`;

export const codeHamming = `const N = 10961;
function sign(key: number, M: number) {
  let s = 1;
  let r = 0;
  for d_i of bits(key) { // d_i ist das ite Bit
    if (d_i) {
      r = (s * M) % N; // time += hammigDistance(s, M)
    } else {
      r = s; // time += 1
    }
    s = (r * r) % N; 
  }
  return s;
}`;
