import { Component } from "@angular/core";

@Component({
  selector: "sca-kocher-explanation",
  templateUrl: "./kocher-explanation.component.html",
  styleUrls: ["./kocher-explanation.component.scss"],
})
export class KocherExplanationComponent {
  public binExpCode = `function bin_exp(x,b) { // Berchnet x^b
  res = 1
  for (i = n..0) {
    res = res**2
    if (b_i == 1) {
      res = res * x
    }
  }
}`;
}
