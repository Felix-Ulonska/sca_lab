import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KocherExplanationComponent } from './kocher-explanation.component';

describe('KocherExplanationComponent', () => {
  let component: KocherExplanationComponent;
  let fixture: ComponentFixture<KocherExplanationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KocherExplanationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KocherExplanationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
