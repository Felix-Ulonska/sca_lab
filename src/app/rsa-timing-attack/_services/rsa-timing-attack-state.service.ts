import { Injectable } from "@angular/core";
import {
  BehaviorSubject,
  combineLatest,
  distinct,
  filter,
  first,
  firstValueFrom,
  map,
  ReplaySubject,
} from "rxjs";
import { GraphData } from "../../components/graph/graph.component";
import { GraphVarianceData } from "../../components/variance-graph/variance-graph.component";
import { ColorService } from "../../shared/_services/color.service";
import {
  Mode,
  OptsRSA,
  RsaScaService,
  Trace,
} from "../../_services/rsa-sca.service";

export interface IAttackState {
  traces: Trace[];
  currentGuessedBits: boolean[];
  key: number;
  rsaOpts: OptsRSA;
  _prev?: IAttackState;
}

@Injectable({
  providedIn: "root",
})
export class RsaTimingAttackStateService {
  attackState$ = new BehaviorSubject<IAttackState | null>(null);

  isAttackRunning$ = this.attackState$.pipe(map((state) => !!state));

  splittedTraces$ = this.attackState$.pipe(
    filter((state) => !!state),
    distinct(),
    map((state) => {
      return this.rsaScaService.splitTraceByBitGuess(
        state!.traces,
        state!.rsaOpts
      );
    })
  );

  tracesMetadata$ = combineLatest([
    this.splittedTraces$,
    this.attackState$,
  ]).pipe(
    distinct(),
    map(([splittedTraces, state]) => {
      return [
        {
          color: this.colorService.getColors().red,
          label: "Current state",
          traces: state!.traces,
          pointStyle: "circle",
        } as GraphData,
        {
          color: this.colorService.getColors().green,
          label: `Trace if d_${state?.currentGuessedBits.length} = 0 bit`,
          traces: splittedTraces.tracesBit0,
          pointStyle: "cross",
        } as GraphData,
        {
          color: this.colorService.getColors().blue,
          label: `Trace if d_${state?.currentGuessedBits.length} = 1 bit`,
          pointStyle: "star",
          traces: splittedTraces.tracesBit1,
        } as GraphData,
      ];
    })
  );

  graphDataVariance$ = combineLatest([
    this.splittedTraces$,
    this.attackState$,
  ]).pipe(
    distinct(),
    map(([splittedTraces, state]) => {
      let curr_state: IAttackState | null | undefined = state;
      let history = [];
      while (curr_state) {
        history.push(curr_state);
        curr_state = curr_state._prev;
      }
      history.reverse();
      return [
        {
          color: this.colorService.getColors().gray[800],
          label: "Current state",
          dataPts: history.map((val, _index) => ({
            x: _index,
            y: this.rsaScaService.addMetadataToTrace(val.traces).var,
          })),
        } as GraphVarianceData,
        {
          color: "blue",
          label: `Var if d_${state?.currentGuessedBits.length} = 1 bit`,
          dataPts: [
            {
              x: history.length,
              y: this.rsaScaService.addMetadataToTrace(
                splittedTraces.tracesBit1
              ).var,
            },
          ],
        } as GraphVarianceData,
        {
          color: "green",
          label: `Var if d_${state?.currentGuessedBits.length} = 0 bit`,
          dataPts: [
            {
              x: history.length,
              y: this.rsaScaService.addMetadataToTrace(
                splittedTraces.tracesBit0
              ).var,
            },
          ],
        } as GraphVarianceData,
      ];
    })
  );

  graphData$ = combineLatest([this.splittedTraces$, this.attackState$]).pipe(
    distinct(),
    map(([splittedTraces, state]) => {
      return [
        //{
        //  color: this.colorService.getColors().red,
        //  label: "Current Durrations",
        //  traces: state!.traces,
        //  pointStyle: "circle",
        //  metadata: this.rsaScaService.addMetadataToTrace(state!.traces),
        //} as GraphData,
        {
          color: this.colorService.getColors().green,
          label: `Restzeit, wenn d_${state?.currentGuessedBits.length} = 0`,
          pointStyle: "triangle",
          traces: splittedTraces.tracesBit0,
          metadata: this.rsaScaService.addMetadataToTrace(
            splittedTraces.tracesBit0
          ),
          pointRadius: 1,
          borderWidth: 5,
        } as GraphData,
        {
          color: this.colorService.getColors().blue,
          label: `Restzeit, wenn d_${state?.currentGuessedBits.length} = 1`,
          pointStyle: "rect",
          traces: splittedTraces.tracesBit1,
          metadata: this.rsaScaService.addMetadataToTrace(
            splittedTraces.tracesBit1
          ),
          pointRadius: 1,
          borderWidth: 5,
        } as GraphData,
        ...this.getHistoryArray(state).map((histState, i) => ({
          label:
            state?.currentGuessedBits.length !==
            histState.currentGuessedBits.length
              ? "HIDE"
              : `Restzeit bei ${histState.currentGuessedBits.length} geratenen bits`,
          pointRadius: 0,
          color: this.colorService.getColors().slate,
          traces: histState.traces,
          metadata: this.rsaScaService.addMetadataToTrace(histState.traces),
          opacity: this.getOpacity(i),
        })),
      ];
    })
  );

  annotations$ = combineLatest([this.splittedTraces$, this.attackState$]).pipe(
    distinct(),
    map(([splittedTraces, state]) => {
      return [
        {
          ...this.rsaScaService.addMetadataToTrace(splittedTraces.tracesBit0),
          label: "traceBit0",
        },
        {
          ...this.rsaScaService.addMetadataToTrace(splittedTraces.tracesBit1),
          label: "traceBit1",
        },
        {
          color: this.colorService.getColors().green,
          label: "Trace 0 bit",
          traces: splittedTraces.tracesBit0,
        } as GraphData,
        {
          color: this.colorService.getColors().blue,
          label: "Trace 1 bit",
          traces: splittedTraces.tracesBit1,
        } as GraphData,
      ];
    })
  );

  constructor(
    private rsaScaService: RsaScaService,
    private colorService: ColorService
  ) {}

  private getOpacity(iter: number) {
    return Math.round((1 / (iter + 1)) * 255).toString(16);
  }

  private getHistoryArray(
    state: IAttackState | undefined | null,
    max: number = -1
  ): IAttackState[] {
    let i = 0;
    let history = [];
    while (state && (i < max || max === -1)) {
      history.push(state);
      state = state._prev;
      i++;
    }
    return history;
  }

  opts: OptsRSA = { mod: 5, tMod: 10, mode: Mode.easy };

  public initNewAttack(opts: {
    numberTraces: number;
    key: number;
    rsaOpts: OptsRSA;
  }) {
    const traces = this.rsaScaService.genTraces(
      opts.numberTraces!,
      opts.key,
      opts.rsaOpts
    );
    this.attackState$.next({
      traces,
      rsaOpts: opts.rsaOpts,
      currentGuessedBits: [],
      key: opts.key,
    });
  }

  public back() {
    this.attackState$.next(this.attackState$.getValue()?._prev ?? null);
  }

  public async next(takeBit: "1" | "0") {
    const state = await firstValueFrom(this.attackState$);
    const splittedTraces = await firstValueFrom(this.splittedTraces$);
    if (!state) {
      throw new Error("Not running attack");
    }

    this.attackState$.next({
      ...state,
      currentGuessedBits: state!.currentGuessedBits.concat([takeBit === "1"]),
      _prev: { ...state },
      traces: (takeBit === "1"
        ? splittedTraces.tracesBit1
        : splittedTraces.tracesBit0
      ).map((trace) => ({ ...trace, pred_t: undefined })),
    });
  }
}
