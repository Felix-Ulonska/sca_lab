import { TestBed } from '@angular/core/testing';

import { RsaTimingAttackStateService } from './rsa-timing-attack-state.service';

describe('RsaTimingAttackStateService', () => {
  let service: RsaTimingAttackStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RsaTimingAttackStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
