# Interactive Web Application for building students' intuition for Side Channel Attacks

## Run the application

- Install Dependencies: Due to a version conflict, `npm i -f` needs to be used
- For Development: Run `ng serve`. Website will be available at `http://localhost:4200/`.
- For Deployment: Run `npm run build`
- For CI/CD is configured to host the application on GitLab Pages.
